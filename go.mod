module dapper-go

go 1.14

require (
	github.com/cbroglie/mustache v1.2.0
	github.com/gobuffalo/flect v0.2.1
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/onsi/ginkgo v1.14.0
	github.com/onsi/gomega v1.10.1
	github.com/sirupsen/logrus v1.6.0
	github.com/valyala/fasttemplate v1.2.0 // indirect
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899 // indirect
	golang.org/x/net v0.0.0-20200707034311-ab3426394381 // indirect
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
	gopkg.in/fsnotify.v1 v1.4.7
	gopkg.in/guregu/null.v4 v4.0.0
	gopkg.in/yaml.v2 v2.3.0
)
