package test

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"time"
)

type Runner struct {
	FixturePath  string
	DatabaseFile *os.File
	Port         int
	Command      *exec.Cmd
	Stdout       io.Writer
	Stderr       io.Writer
}

func NewRunner(basePath string) *Runner {
	return &Runner{
		Port:        8080,
		FixturePath: basePath + "/fixtures/db.yml",
	}
}

func (r *Runner) Start(basePath string) error {
	file, err := ioutil.TempFile("", "dapper")
	if err != nil {
		return err
	}

	fixtureContents, err := ioutil.ReadFile(r.FixturePath)
	if err := ioutil.WriteFile(file.Name(), fixtureContents, os.FileMode(400)); err != nil {
		return err
	}

	r.DatabaseFile = file

	r.Command = exec.Command(basePath+"/dapper", "-database", r.DatabaseFile.Name(), string(r.Port))
	r.Command.Stdout = r.Stdout
	r.Command.Stderr = r.Stderr

	if err := r.Command.Start(); err != nil {
		return err
	}

	return nil
}

func (r *Runner) ListenAddress() string {
	return fmt.Sprintf(":%d", r.Port)
}

func (r *Runner) URI(uri string, replacements ...interface{}) string {
	uri = fmt.Sprintf(uri, replacements...)
	return fmt.Sprintf("http://%s%s", r.ListenAddress(), uri)
}

func (r *Runner) WaitForStartup(timeout time.Duration) error {
	if err := waitForServices([]string{r.ListenAddress()}, timeout); err != nil {
		return err
	}

	return nil
}

func (r *Runner) Stop() error {
	// close and remove temporary database file
	r.DatabaseFile.Close()
	os.Remove(r.DatabaseFile.Name())

	// kill dapper
	if err := r.Command.Process.Kill(); err != nil {
		return err
	}

	// wait until process exits
	if _, err := r.Command.Process.Wait(); err != nil {
		return err
	}

	return nil
}
