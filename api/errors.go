package api

import (
	"dapper-go/errors"
	"fmt"
	"net/http"

	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
)

func handleError(err error, c echo.Context) {
	errorEntry, isType := err.(errors.ErrorEntry)

	// if error is not an ErrorEntry type
	if !isType {
		c.Logger().Error(err)
		c.Echo().DefaultHTTPErrorHandler(err, c)
		return
	}

	// interface{} can only be coerced to a float64, and then to an integer
	httpCode, ok := errorEntry.Meta["http_status"].(float64)
	if !ok || httpCode <= 0 {
		httpCode = http.StatusInternalServerError
	}

	c.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSONCharsetUTF8)
	c.Response().Header().Set("Dapper-Code", fmt.Sprintf("%v", errorEntry.Code))
	c.JSON(int(httpCode), map[string]interface{}{
		"error":          errorEntry,
		"original_error": errorEntry.OriginalErrorMessage,
	})

	logrus.WithFields(logrus.Fields{
		"Code":          errorEntry.Code,
		"Message":       errorEntry.Message,
		"OriginalError": errorEntry.OriginalErrorMessage,
	}).Warningln(err)
}
