package errors

import (
	"encoding/json"
	"fmt"
)

const (
	// DbFileMissing - Database file %q does not exist
	DbFileMissing = "db_file_missing"
	// DbFileUnreadable - Database file %q cannot be read
	DbFileUnreadable = "db_file_unreadable"
	// DbFileUnwriteable - Database file %q cannot be written
	DbFileUnwriteable = "db_file_unwriteable"
	// DbUnwatchable - Error watching database file %q for changes
	DbUnwatchable = "db_unwatchable"
	// EntriesDbParse - Could not parse entries database file %q
	EntriesDbParse = "entries_db_parse"
	// FailedDelete - Failed to delete given entry
	FailedDelete = "failed_delete"
	// FailedUpsert - Failed to upsert given entry
	FailedUpsert = "failed_upsert"
	// InvalidCode - Invalid entry code provided in request
	InvalidCode = "invalid_code"
	// MissingCode - Entry code not provided in request
	MissingCode = "missing_code"
	// MissingEntry - Cannot find error entry by code
	MissingEntry = "missing_entry"
	// MissingRequestParameters - Missing parameters %q in request
	MissingRequestParameters = "missing_request_parameters"
	// ParameterBind - Failed to bind given parameters to entry struct
	ParameterBind = "parameter_bind"
	// RenderedTemplateStorageFailed - Could not write rendered template to temporary storage
	RenderedTemplateStorageFailed = "rendered_template_storage_failed"
	// SerializationFailed - Failed to serialize data
	SerializationFailed = "serialization_failed"
	// TemplateReadFailed - Could not read given template
	TemplateReadFailed = "template_read_failed"
	// TemplateRenderFailed - Could not render given template
	TemplateRenderFailed = "template_render_failed"
)

// ErrorEntry describes the structure of an error entry
type ErrorEntry struct {
	error

	// original error
	OriginalError        error  `json:"-"`
	OriginalErrorMessage string `json:"-"`

	Code        string                 `json:"code"`
	Message     string                 `json:"message"`
	Description string                 `json:"description,omitempty"`
	Cause       string                 `json:"cause,omitempty"`
	Solution    string                 `json:"solution,omitempty"`
	Meta        map[string]interface{} `json:"meta,omitempty"`
}

func (e ErrorEntry) Error() string {
	return fmt.Sprintf("[DAPPER-%v] %v", e.Code, e.Message)
}

// GetErrorByCode retrieves an error entry by its given numeric code
func GetErrorByCode(code string) ErrorEntry {
	var content string
	var entry ErrorEntry

	switch code {
	case DbFileMissing:
		content = `{"message":"Database file %q does not exist","description":null,"cause":"Possible incorrect path given","solution":"Check the given path for errors, and ensure the file is accessible/readable by this process","meta":{},"tags":["general"],"code":"db_file_missing","created":1594563993,"updated":1594563993}`
	case DbFileUnreadable:
		content = `{"message":"Database file %q cannot be read","description":null,"cause":"Given path is unreadable by this process","solution":"Check the given path for errors, and ensure the file is accessible/readable by this process","meta":{},"tags":["general"],"code":"db_file_unreadable","created":1594564168,"updated":1594564168}`
	case DbFileUnwriteable:
		content = `{"message":"Database file %q cannot be written","description":null,"cause":"Possible permissions issue","solution":"Check the given path for errors, and ensure the file is accessible/writeable by this process","meta":{},"tags":["general"],"code":"db_file_unwriteable","created":1594564577,"updated":1594564577}`
	case DbUnwatchable:
		content = `{"message":"Error watching database file %q for changes","description":null,"cause":"Possible file access issue","solution":"Consult the application logs","meta":{},"tags":["general"],"code":"db_unwatchable","created":1594566477,"updated":1594566477}`
	case EntriesDbParse:
		content = `{"message":"Could not parse entries database file %q","description":null,"cause":"Possible syntax or validation error","solution":"Check entries database file for syntax or validation errors","meta":{},"tags":["validation"],"code":"entries_db_parse","created":1594563670,"updated":1594563689}`
	case FailedDelete:
		content = `{"message":"Failed to delete given entry","description":null,"cause":"Possible issues writing to database file","solution":"Consult the application logs","meta":{"http_status":500},"tags":["general"],"code":"failed_delete","created":1594561159,"updated":1594561159}`
	case FailedUpsert:
		content = `{"message":"Failed to upsert given entry","description":null,"cause":"Possible issues writing to database file","solution":"Consult the application logs","meta":{"http_status":500},"tags":["general"],"code":"failed_upsert","created":1594561159,"updated":1595172064}`
	case InvalidCode:
		content = `{"message":"Invalid entry code provided in request","description":null,"cause":"Parameter is not a 32-bit unsigned integer","solution":"Provide entry code as a 32-bit unsigned integer","meta":{"http_status":400},"tags":["validation"],"code":"invalid_code","created":1594557022,"updated":1594557059}`
	case MissingCode:
		content = `{"message":"Entry code not provided in request","description":null,"cause":"Parameter not provided in request","solution":"Provide the parameter in the request","meta":{"http_status":400},"tags":["validation"],"code":"missing_code","created":1594557022,"updated":1594557059}`
	case MissingEntry:
		content = `{"message":"Cannot find error entry by code","description":null,"cause":"Error entry does not exist","solution":"None","meta":{"http_status":404},"tags":["general"],"code":"missing_entry","created":1594558082,"updated":1594558082}`
	case MissingRequestParameters:
		content = `{"message":"Missing parameters %q in request","description":null,"cause":"Missing parameters in request","solution":"Include the required parameters","meta":{"http_status":400},"tags":["validation"],"code":"missing_request_parameters","created":1594561636,"updated":1594561636}`
	case ParameterBind:
		content = `{"message":"Failed to bind given parameters to entry struct","description":null,"cause":"Possible validation errors in given data","solution":"Consult the documentation and correct problematic parameters","meta":{"http_status":400},"tags":["validation"],"code":"parameter_bind","created":1594560991,"updated":1594560991}`
	case RenderedTemplateStorageFailed:
		content = `{"message":"Could not write rendered template to temporary storage","description":null,"cause":"Possible full disk","solution":"Ensure that enough free space is allocated to system temporary directory","meta":{},"tags":["general"],"code":"rendered_template_storage_failed","created":1594562195,"updated":1594562195}`
	case SerializationFailed:
		content = `{"message":"Failed to serialize data","description":null,"cause":"Invalid data given","solution":"Check given data for validation issues","meta":{"http_status":500},"tags":["general"],"code":"serialization_failed","created":1594547407,"updated":1594547447}`
	case TemplateReadFailed:
		content = `{"message":"Could not read given template","description":null,"cause":"Possible issues with supplied template","solution":"Review given template that is being submitted in the request","meta":{},"tags":["general"],"code":"template_read_failed","created":1594562023,"updated":1594562023}`
	case TemplateRenderFailed:
		content = `{"message":"Could not render given template","description":null,"cause":"Possible issues with supplied template","solution":"Review given template that is being submitted in the request","meta":{},"tags":["general"],"code":"template_render_failed","created":1594562103,"updated":1594562103}`
	default:
		content = `{"message": "Unknown error"}`
	}

	err := json.Unmarshal([]byte(content), &entry)
	if err != nil {
		return ErrorEntry{
			Message: "Unknown error",
			Cause:   "Internal parsing error",
		}
	}

	return entry
}

// GetErrorByCodeWithOriginal retrieves an error entry by its given numeric code,
// and tracks the original error
func GetErrorByCodeWithOriginal(code string, original error) ErrorEntry {
	entry := GetErrorByCode(code)
	entry.OriginalError = original
	entry.OriginalErrorMessage = original.Error()

	return entry
}
