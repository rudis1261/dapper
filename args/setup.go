package args

import "flag"

var List arguments

func Setup() {
	List = arguments{
		Database: flag.String("database", "entries.json", "database file (JSON)"),
		Port:     flag.Int("port", 8080, "listen address"),
	}

	flag.Parse()
}

type arguments struct {
	Port     *int
	Database *string
}
