package codegen

import (
	"dapper-go/db"
	"strings"

	"github.com/gobuffalo/flect"
)

func constantizeGo(entry *db.Entry) string {
	return pascalizeConstants(entry)
}

func constantizePHP(entry *db.Entry) string {
	return uppercaseConstants(entry)
}

func pascalizeConstants(entry *db.Entry) string {
	return flect.Pascalize(string(entry.Code))
}

func uppercaseConstants(entry *db.Entry) string {
	return strings.ToUpper(string(entry.Code))
}
