package db

import (
	"dapper-go/errors"
	"fmt"
	"sort"
	"time"

	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

func (db *Database) loadEntries(contents []byte) {
	// ensure we don't encounter a race-condition when trying to both
	// read from and write to our database instance in memory
	writeLock()
	defer writeUnlock()

	err := yaml.Unmarshal(contents, &db)
	if err != nil {
		errorEntry := errors.GetErrorByCodeWithOriginal(errors.EntriesDbParse, err)
		errorEntry.Message = fmt.Sprintf(errorEntry.Message, db.Path)

		logrus.Warningln(errorEntry)
		return
	}

	fillEntryCodes(db.Entries)
}

func (db *Database) serializeEntries() ([]byte, error) {
	return yaml.Marshal(db)
}

// getEntriesCopy orders the database by code, and returns a sorted copy of the database
func (db *Database) getEntriesCopy() map[EntryCode]*Entry {
	codes := db.SortedCodes()

	var clone = make(map[EntryCode]*Entry)

	for _, code := range codes {
		code := EntryCode(code)
		entry := db.Entries[code]

		clone[code] = entry
	}

	fillEntryCodes(clone)
	return clone
}

// setEntryCodes sets each Entry's `Code` property of a given map
// since this value is the key of the map, but we need it
// elsewhere as a property of each Entry
func fillEntryCodes(entries map[EntryCode]*Entry) {
	for code, entry := range entries {
		entry.Code = code
	}
}

func (db *Database) SortedCodes() []string {
	var codes []string

	for code := range db.Entries {
		codes = append(codes, string(code))
	}

	sort.Strings(codes)
	return codes
}

// GetEntries lists all entries
func (db *Database) GetEntries() map[EntryCode]*Entry {
	readLock()
	defer readUnlock()

	// returns a sorted copy of the database
	// NOTE: a copy is important here since the caller might want to
	// reference the map later, and since the database is a singleton,
	// this could lead to a race
	return db.getEntriesCopy()
}

// GetEntryByCode looks up an entry by code
func (db *Database) GetEntryByCode(code EntryCode) (*Entry, error) {
	readLock()
	defer readUnlock()

	entry, found := db.Entries[code]
	if !found {
		return nil, fmt.Errorf("cannot find entry by code: %v", code)
	}

	entry.Code = code
	return entry, nil
}

// UpsertEntry inserts or updates an entry, and then flushes it to disk
func (db *Database) UpsertEntry(code EntryCode, entry *Entry) (err error) {
	entry.Code = code
	entry.UpdatedAt = time.Now().Unix()

	writeLock()
	defer writeUnlock()
	db.Entries[code] = entry

	return db.Write()
}

// DeleteEntry removes an entry
func (db *Database) DeleteEntry(code EntryCode) (err error) {
	writeLock()
	defer writeUnlock()
	delete(db.Entries, code)

	return db.Write()
}

// GetTranslatedEntryByCode looks up an entry's translation by code
// func (db *Database) GetTranslatedEntryByCode(code int, translationCode string) (*Entry, error) {
// 	entry, err := db.GetEntryByCode(code)
// 	if err != nil {
// 		return nil, err
// 	}

// 	translation, found := entry.Translations[TranslationCode(translationCode)]
// 	if !found {
// 		return nil, fmt.Errorf("cannot find translated entry by code: %v", code)
// 	}

// 	return entry.ApplyTranslation(translation), nil
// }
