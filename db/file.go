package db

import (
	"dapper-go/errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sync"

	"github.com/sirupsen/logrus"
)

var instance *Database
var once sync.Once
var mutex sync.RWMutex

// Instance creates a new database singleton instance
func Instance() *Database {
	once.Do(func() {
		instance = &Database{}
	})

	return instance
}

func (db *Database) Read() {
	logrus.WithField("path", db.Path).Infoln("reading database file")
	db.read()
}

func (db *Database) Write() error {
	logrus.WithField("path", db.Path).Infoln("writing database file")
	return db.write()
}

func readLock() {
	mutex.RLock()
}

func readUnlock() {
	mutex.RUnlock()
}

func writeLock() {
	mutex.Lock()
}

func writeUnlock() {
	mutex.Unlock()
}

func (db *Database) read() {
	if !db.pathExists() {
		errorEntry := errors.GetErrorByCode(errors.DbFileMissing)
		errorEntry.Message = fmt.Sprintf(errorEntry.Message, db.Path)

		logrus.Fatalln(errorEntry)
	}

	contents, err := ioutil.ReadFile(db.Path)
	if err != nil {
		errorEntry := errors.GetErrorByCodeWithOriginal(errors.DbFileUnreadable, err)
		errorEntry.Message = fmt.Sprintf(errorEntry.Message, db.Path)

		logrus.Fatalln(errorEntry, errorEntry.OriginalErrorMessage)
	}

	db.loadEntries(contents)
}

func (db *Database) write() error {
	if !db.pathExists() {
		errorEntry := errors.GetErrorByCode(errors.DbFileMissing)
		errorEntry.Message = fmt.Sprintf(errorEntry.Message, db.Path)

		logrus.Fatalln(errorEntry)
	}

	contents, err := db.serializeEntries()
	if err != nil {
		return errors.GetErrorByCodeWithOriginal(errors.SerializationFailed, err)
	}

	stat, err := os.Stat(db.Path)
	if err != nil {
		errorEntry := errors.GetErrorByCodeWithOriginal(errors.DbFileUnreadable, err)
		errorEntry.Message = fmt.Sprintf(errorEntry.Message, db.Path)

		logrus.Fatalln(errorEntry, errorEntry.OriginalErrorMessage)
	}

	err = ioutil.WriteFile(db.Path, contents, stat.Mode())
	if err != nil {
		errorEntry := errors.GetErrorByCodeWithOriginal(errors.DbFileUnwriteable, err)
		errorEntry.Message = fmt.Sprintf(errorEntry.Message, db.Path)
		return errorEntry
	}

	return nil
}

func (db *Database) save(contents []byte) (err error) {
	return ioutil.WriteFile(db.Path, contents, 0644)
}

func (db *Database) pathExists() bool {
	info, err := os.Stat(filepath.FromSlash(db.Path))

	if os.IsNotExist(err) {
		return false
	}

	return !info.IsDir()
}
